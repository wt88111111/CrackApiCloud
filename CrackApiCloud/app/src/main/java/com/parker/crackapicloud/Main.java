package com.parker.crackapicloud;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

import com.saurik.substrate.MS;

import mirror.android.app.ActivityThread;

/**
 * Created by parker on 2017/7/20.
 */
public class Main {
    private final static String TAG = "roosee";
    private static Context context;
    private static AtomicBoolean hasCrackRun = new AtomicBoolean(false);
    static void initialize() {



        MS.hookClassLoad("com.uzmap.pkg.uzcore.b.d", new MS.ClassLoadHook() {
            @Override
            public void classLoaded(Class<?> aClass) {
/*
                Method[] methods = aClass.getDeclaredMethods();
                for (Method method : methods){
                    Log.d(TAG,"method:"+method.toGenericString());
                }
*/
                try {
                    Method secLoadUrl = aClass.getDeclaredMethod("c", new Class<?>[]{String.class});
                    Log.d(TAG, "success to find:" + secLoadUrl.toGenericString());

                    if (context == null){
                        Object  currentActivityThread = ActivityThread.currentActivityThread.call();
                        Application app = ActivityThread.mInitialApplication.get(currentActivityThread);
                        context = app.getApplicationContext();
                    }

                    final MS.MethodPointer old = new MS.MethodPointer();
                    MS.hookMethod(aClass, secLoadUrl, new MS.MethodHook() {
                        @Override
                        public Object invoked(Object o, Object... args) throws Throwable {
                            String url = (String) args[0];
                            if (!hasCrackRun.get()){
                                new Thread(new CrackRunnable(context, o, old)).start();
                                hasCrackRun.set(true);
                            }
                            //Log.d(TAG, "secLoadUrl:" + url);
                            return old.invoke(o,args);
                        }
                    }, old);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
    }
}
