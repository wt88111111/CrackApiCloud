package com.parker.crackapicloud;

import android.content.Context;
import android.util.Log;

import com.saurik.substrate.MS;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashSet;

/**
 * Created by parker on 2017/7/20.
 */

public class CrackRunnable implements Runnable{
    private final static String TAG = "roosee";
    private Context context;
    private Object o;
    private MS.MethodPointer secLoadUrl;
    private static HashSet<String> exts = new HashSet<>();

    static{
        exts.add("js");
        exts.add("html");
        exts.add("css");
        exts.add("xml");
    }

    public CrackRunnable(Context context, Object o, MS.MethodPointer secLoadUrl){
        this.context = context;
        this.o = o;
        this.secLoadUrl = secLoadUrl;
    }

    private String makeUri(String path){
        return "///data/data/" + context.getPackageName() + "/" + path;
    }

    private boolean listAssetFilesAndCrack(String path) {

        String [] list;
        try {
            list = context.getAssets().list(path);
            if (list.length > 0) {
                // This is a folder
                for (String file : list) {
                    if (!listAssetFilesAndCrack(path + "/" + file))
                        return false;
                }
            } else {
                //  ///data/data/com.cheshen.zhibo/widget/css/api.css
                String ext = path.substring(path.lastIndexOf(".") + 1);
                byte[] data = null;
                if (exts.contains(ext.toLowerCase())){
                    String uri = makeUri(path);
                    try {
                        if (this.secLoadUrl != null){
                            Log.d(TAG, "cracking: " + uri);
                            data = (byte[])secLoadUrl.invoke(this.o, uri);
                        }
                    } catch (Throwable e) {
                        Log.e(TAG, e.toString());
                        e.printStackTrace();
                    }
                }else{
                    InputStream in = context.getAssets().open(path);
                    data = IOUtils.toByteArray(in);
                }

                if (data != null && data.length > 0){
                    File destFile = new File(context.getFilesDir(), path);
                    if (!destFile.getParentFile().exists()){
                        destFile.getParentFile().mkdirs();
                    }
                    Log.d(TAG, "saving " + path + "---->" + destFile.getCanonicalPath());
                    ByteArrayInputStream bais = new ByteArrayInputStream(data);
                    FileUtils.copyToFile(bais, destFile);
                    bais.close();
                }else{
                    Log.e(TAG, path + "`s data is null!");
                }

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return false;
        }

        return true;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "CrackRunnable is running!");
        listAssetFilesAndCrack("widget");
        Log.d(TAG, "CrackRunnable is gonna exit!");

    }
}
